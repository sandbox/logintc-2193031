LoginTC
=======

Introduction
------------

This module adds an additional layer of security to the authentication of users
on your Drupal site using LoginTC two-factor authentication. Using this module,
a user enters their username and password and then they must approve a request
on their mobile device to complete the authentication process. Even if a hacker
gets the user's password, they won't be able to authenticate without the user's
mobile device.

Requirements
------------

 * Drupal 7.0+
 * cURL PHP extension
 * JSON PHP extension

Documentation
-------------

See <https://www.logintc.com/docs/connectors/drupal.html> for full instructions.

Help
----

Email: <support@cyphercor.com>
